# Pull down chart from oci registry
```bash
helm registry login registry.gitlab.com
helm pull oci://registry.gitlab.com/stargeras/helm-freeipa/freeipa --version=1.0.0
```

# Sample terraform code to deploy from OCI registry
```terraform
resource "helm_release" "freeipa" {
  name             = "freeipa"
  namespace        = "freeipa"
  repository       = "oci://registry.gitlab.com"
  version          = "1.0.0"
  chart            = "stargeras/helm-freeipa/freeipa"
  create_namespace = true
}
```
